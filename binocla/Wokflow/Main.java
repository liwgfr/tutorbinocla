package Wokflow;

import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        int min = Stream.of(1,5,2,3,21,23).reduce((a, b) -> a.compareTo(b) >= 0 ? a : b).get();
        System.out.println(min);
    }
}

